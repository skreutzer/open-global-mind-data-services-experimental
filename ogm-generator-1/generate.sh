#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of ogm-generator-1.
#
# ogm-generator-1is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# ogm-generator-1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with ogm-generator-1. If not, see <http://www.gnu.org/licenses/>.

mkdir -p ./output/
mkdir -p ./temp/

java -cp ./digital_publishing_workflow_tools/csv_to_xml/csv_to_xml_1/ csv_to_xml_1 ./jobfile_csv_to_xml_1.xml ./resultinfo_csv_to_xml_1.xml
java -cp ./prepare_input_1/ prepare_input_1 ./temp/input.xml ./temp/input_prepared.xml
java -cp ./automated_digital_publishing/xml_split/xml_split1/ xml_split1 ./temp/input_prepared.xml ./automated_digital_publishing/xml_split/xml_split1/entities/config.xml ./jobfile_xml_split_1.xml ./temp/input/
rm ./temp/input/info.xml
java -cp ./digital_publishing_workflow_tools/file_discovery/file_discovery_1/ file_discovery_1 ./jobfile_file_discovery_1.xml ./resultinfo_file_discovery_1.xml
java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1_1.xml ./resultinfo_xml_xslt_transformator_1_1.xml
java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./temp/jobfile_xml_xslt_transformator_1_2.xml ./resultinfo_xml_xslt_transformator_1_2.xml
