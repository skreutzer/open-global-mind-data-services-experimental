<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2020  Stephan Kreutzer

This file is part of ogm-generator-1.

ogm-generator-1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

ogm-generator-1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with ogm-generator-1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">
    <xml-xslt-transformator-1-jobfile>
      <xsl:comment> This file was created by section_files_index_to_xml_xslt_transformator_1_jobfile.xsl of ogm-generator-1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/open-global-mind-data-services-experimental/ and https://publishing-systems.org). </xsl:comment>
      <xsl:apply-templates select="./entry-list/entries/entry"/>
    </xml-xslt-transformator-1-jobfile>
  </xsl:template>

  <xsl:template match="/entry-list/entries/entry">
    <job input-file="{@path}" entities-resolver-config-file="../digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/entities/config_empty.xml" stylesheet-file="../section_to_xhtml_1.xsl">
      <xsl:attribute name="output-file">
        <xsl:text>../output/</xsl:text>
        <xsl:number count="/entry-list/entries/entry" level="any"/>
        <xsl:text>.xhtml</xsl:text>
      </xsl:attribute>
    </job>
  </xsl:template>

  <xsl:template match="text()|node()|@*"/>

</xsl:stylesheet>
