<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2020  Stephan Kreutzer

This file is part of ogm-generator-1.

ogm-generator-1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

ogm-generator-1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with ogm-generator-1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
        <xsl:text>&#x0A;</xsl:text>
        <xsl:comment> This file was created by section_to_xhtml_1.xsl of ogm-generator-1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/open-global-mind-data-services-experimental/ and https://publishing-systems.org). </xsl:comment>
        <xsl:text>&#x0A;</xsl:text>
        <title>
          <xsl:text>OGM Clip</xsl:text>
        </title>
        <style type="text/css">
          body
          {
              font-family: sans-serif;
          }
        </style>
      </head>
      <body>
        <div>
          <h1>
            <xsl:text>OGM Clip</xsl:text>
          </h1>
          <div>
            <xsl:apply-templates select="./section"/>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="/section">
    <div>
      <div>
        <iframe xmlns="http://www.w3.org/1999/xhtml" scrolling="no" marginheight="0" marginwidth="0" type="text/html" width="788.54" height="443" frameborder="0">
          <xsl:attribute name="src">
            <xsl:text>https://www.youtube.com/embed/VOvw30-mCM4?autoplay=0&amp;fs=0&amp;iv_load_policy=3&amp;showinfo=1&amp;rel=0&amp;cc_load_policy=0&amp;start=</xsl:text>
            <xsl:value-of select="./start/seconds/text()"/>
            <xsl:text>&amp;end=</xsl:text>
            <xsl:value-of select="./end/seconds/text()"/>
          </xsl:attribute>
        </iframe>
        <div>
          <xsl:value-of select="./start/timecode/text()"/>
          <xsl:text>-</xsl:text>
          <xsl:value-of select="./end/timecode/text()"/>
        </div>
      </div>
      <div>
        <xsl:apply-templates select="./text"/>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="/section/text">
    <p>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="/section/text//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="text()|node()|@*"/>

</xsl:stylesheet>
