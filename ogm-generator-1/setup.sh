#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of ogm-generator-1.
#
# ogm-generator-1is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# ogm-generator-1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with ogm-generator-1. If not, see <http://www.gnu.org/licenses/>.

sudo apt-get install default-jre unzip wget

sleep 3s; wget https://gitlab.com/skreutzer/open-global-mind-data-repository-experimental/-/archive/master/open-global-mind-data-repository-experimental-master.zip
unzip ./open-global-mind-data-repository-experimental-master.zip
mv ./open-global-mind-data-repository-experimental-master/ ./open-global-mind-data-repository-experimental/
rm ./open-global-mind-data-repository-experimental-master.zip
