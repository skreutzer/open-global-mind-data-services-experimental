/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of prepare_input_1, a submodule of the
 * open-global-mind-data-services-experimental/ogm-generator-1 package.
 *
 * open-global-mind-data-services-experimental/ogm-generator-1 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * open-global-mind-data-services-experimental/ogm-generator-1 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with open-global-mind-data-services-experimental/ogm-generator-1. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/prepare_input_1/prepare_input_1.java
 * @author Stephan Kreutzer
 * @since 2020-08-02
 */



import java.io.File;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import javax.xml.stream.XMLInputFactory;
import java.io.InputStream;
import java.io.FileInputStream;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;
import javax.xml.namespace.QName;
import java.util.Iterator;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.ProcessingInstruction;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.EntityReference;
import javax.xml.stream.events.Comment;
import javax.xml.stream.events.DTD;
import javax.xml.stream.XMLStreamException;
import java.io.UnsupportedEncodingException;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;



public class prepare_input_1
{
    public static void main(String args[])
    {
        System.out.print("prepare_input_1 Copyright (C) 2020 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/skreutzer/open-global-mind-data-services-experimental/.\n\n");

        if (args.length < 2)
        {
            System.out.println("Usage:\n\tprepare_input_1 <input-file> <output-file>\n");
            System.exit(1);
        }


        File inputFile = new File(args[0]);

        try
        {
            inputFile = inputFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            System.out.println("prepare_input_1: Can't get canonical path of input file \"" + inputFile.getAbsolutePath() + "\".");
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("prepare_input_1: Can't get canonical path of input file \"" + inputFile.getAbsolutePath() + "\".");
            ex.printStackTrace();
            System.exit(1);
        }

        if (inputFile.exists() != true)
        {
            System.out.println("prepare_input_1: Input file \"" + inputFile.getAbsolutePath() + "\" doesn't exist.");
            System.exit(1);
        }

        if (inputFile.isFile() != true)
        {
            System.out.println("prepare_input_1: Input path \"" + inputFile.getAbsolutePath() + "\" isn't a file.");
            System.exit(1);
        }

        if (inputFile.canRead() != true)
        {
            System.out.println("prepare_input_1: Input file \"" + inputFile.getAbsolutePath() + "\" isn't readable.");
            System.exit(1);
        }

        File outputFile = new File(args[1]);

        try
        {
            outputFile = outputFile.getCanonicalFile();
        }
        catch (SecurityException ex)
        {
            System.out.println("prepare_input_1: Can't get canonical path of output file \"" + inputFile.getAbsolutePath() + "\".");
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("prepare_input_1: Can't get canonical path of output file \"" + inputFile.getAbsolutePath() + "\".");
            ex.printStackTrace();
            System.exit(1);
        }

        if (outputFile.exists() == true)
        {
            if (outputFile.isFile() == true)
            {
                if (outputFile.canWrite() != true)
                {
                    System.out.println("prepare_input_1: Output file \"" + outputFile.getAbsolutePath() + "\" isn't writable.");
                    System.exit(1);
                }
            }
            else
            {
                System.out.println("prepare_input_1: Output path \"" + outputFile.getAbsolutePath() + "\" isn't a file.");
                System.exit(1);
            }
        }


        DecimalFormat formatTwoDigits = new DecimalFormat("00");

        try
        {
            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(
                                    new FileOutputStream(outputFile),
                                    "UTF-8"));

            try
            {
                XMLInputFactory inputFactory = XMLInputFactory.newInstance();

                // This is a concatenator for generic XML, don't want to deal with legacy DTD
                // remnants. Consider using $/xml_dtd_entity_resolver/xml_dtd_entity_resolver_1.
                inputFactory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
                inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);

                InputStream in = new FileInputStream(inputFile);
                XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

                boolean inTimecode = false;

                while (eventReader.hasNext() == true)
                {
                    XMLEvent event = eventReader.nextEvent();

                    if (event.isStartDocument() == true)
                    {
                        StartDocument startDocument = (StartDocument)event;

                        writer.write("<?xml version=\"" + startDocument.getVersion() + "\"");

                        if (startDocument.encodingSet() == true)
                        {
                            writer.write(" encoding=\"" + startDocument.getCharacterEncodingScheme() + "\"");
                        }

                        if (startDocument.standaloneSet() == true)
                        {
                            writer.write(" standalone=\"");

                            if (startDocument.isStandalone() == true)
                            {
                                writer.write("yes");
                            }
                            else
                            {
                                writer.write("no");
                            }

                            writer.write("\"");
                        }

                        writer.write("?>");
                    }
                    else if (event.isStartElement() == true)
                    {
                        QName elementName = event.asStartElement().getName();
                        String fullElementName = elementName.getLocalPart();

                        if (elementName.getPrefix().isEmpty() != true)
                        {
                            fullElementName = elementName.getPrefix() + ":" + fullElementName;
                        }

                        writer.write("<" + fullElementName);

                        // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                        @SuppressWarnings("unchecked")
                        Iterator<Namespace> namespaces = (Iterator<Namespace>)event.asStartElement().getNamespaces();

                        while (namespaces.hasNext() == true)
                        {
                            Namespace namespace = namespaces.next();

                            if (namespace.isDefaultNamespaceDeclaration() == true &&
                                namespace.getPrefix().length() <= 0)
                            {
                                writer.write(" xmlns=\"" + namespace.getNamespaceURI() + "\"");
                            }
                            else
                            {
                                writer.write(" xmlns:" + namespace.getPrefix() + "=\"" + namespace.getNamespaceURI() + "\"");
                            }
                        }

                        // http://coding.derkeiler.com/Archive/Java/comp.lang.java.help/2008-12/msg00090.html
                        @SuppressWarnings("unchecked")
                        Iterator<Attribute> attributes = (Iterator<Attribute>)event.asStartElement().getAttributes();

                        while (attributes.hasNext() == true)
                        {
                            Attribute attribute = attributes.next();
                            QName attributeName = attribute.getName();
                            String fullAttributeName = attributeName.getLocalPart();

                            if (attributeName.getPrefix().length() > 0)
                            {
                                fullAttributeName = attributeName.getPrefix() + ":" + fullAttributeName;
                            }

                            String attributeValue = attribute.getValue();

                            // Ampersand needs to be the first, otherwise it would double-encode
                            // other entities.
                            attributeValue = attributeValue.replaceAll("&", "&amp;");
                            attributeValue = attributeValue.replaceAll("\"", "&quot;");
                            attributeValue = attributeValue.replaceAll("'", "&apos;");
                            attributeValue = attributeValue.replaceAll("<", "&lt;");
                            attributeValue = attributeValue.replaceAll(">", "&gt;");

                            writer.write(" " + fullAttributeName + "=\"" + attributeValue + "\"");
                        }

                        writer.write(">");

                        if (fullElementName.equals("start") == true ||
                            fullElementName.equals("end") == true)
                        {
                            inTimecode = true;
                        }
                    }
                    else if (event.isEndElement() == true)
                    {
                        QName elementName = event.asEndElement().getName();
                        String fullElementName = elementName.getLocalPart();

                        if (elementName.getPrefix().isEmpty() != true)
                        {
                            fullElementName = elementName.getPrefix() + ":" + fullElementName;
                        }

                        writer.write("</" + fullElementName + ">");

                        if (fullElementName.equals("start") == true ||
                            fullElementName.equals("end") == true)
                        {
                            inTimecode = false;
                        }
                    }
                    else if (event.isCharacters() == true)
                    {
                        if (inTimecode == true)
                        {
                            String characters = event.asCharacters().getData();

                            // Also, java.time is only available from Java 1.8,
                            // and this is supposed to be compatible with Java 1.7.

                            int[] numbers = { -1, -1, -1 };
                            String[] time = characters.split(":");

                            if (time.length <= 0)
                            {
                                System.out.println("prepare_input_1: Timecode is empty or invalid.");
                                System.exit(1);
                            }

                            for (int i = time.length - 1; i >= 0; i--)
                            {
                                int j = (time.length - 1) - i;

                                int number = -1;

                                try
                                {
                                    number = Integer.parseInt(time[i]);
                                }
                                catch (NumberFormatException ex)
                                {
                                    System.out.println("prepare_input_1: An error occurred while trying to parse a part of \"" + characters + "\" of the timecode as an integer.");
                                    ex.printStackTrace();
                                    System.exit(1);
                                }

                                if (i < time.length - 3)
                                {
                                    break;
                                }

                                if (number < 0 || number > 59)
                                {
                                    if (i == time.length - 1)
                                    {
                                        System.out.println("prepare_input_1: Number of second from \"" + characters + "\" of the timecode isn't valid.");
                                        System.exit(1);
                                    }
                                    else if (i == time.length - 2)
                                    {
                                        System.out.println("prepare_input_1: Number of minute from \"" + characters + "\" of the timecode isn't valid.");
                                        System.exit(1);
                                    }
                                    else if (i == time.length - 3)
                                    {

                                    }
                                }

                                numbers[j] = number;
                            }


                            
                            
                            
                            
                            /*
                            for (int i = 0; i < numbers.length; i++)
                            {
                                if (i == 0)
                                {
                                    numbers[i] += 1;

                                    if (numbers[i] >= 60)
                                    {
                                        numbers[i] -= 60;

                                        if (numbers[i+1] < 0)
                                        {
                                            numbers[i+1] = 0;
                                        }

                                        numbers[i+1] += 1;
                                    }
                                }
                                else if (i == 1)
                                {
                                    if (numbers[i] >= 60)
                                    {
                                        numbers[i] -= 60;

                                        if (numbers[i+1] < 0)
                                        {
                                            numbers[i+1] = 0;
                                        }

                                        numbers[i+1] += 1;
                                    }
                                }
                                else
                                {

                                }
                            }
                            */

                            writer.write("<timecode>");

                            for (int i = numbers.length - 1; i >= 0; i--)
                            {
                                if (numbers[i] >= 0)
                                {
                                    if (i == 0 ||
                                        i == 1)
                                    {
                                        writer.write(formatTwoDigits.format(numbers[i]));
                                    }
                                    else
                                    {
                                        writer.write(Integer.toString(numbers[i]));
                                    }

                                    if (i > 0)
                                    {
                                        writer.write(":");
                                    }
                                }
                            }

                            writer.write("</timecode>");

                            long seconds = 0L;

                            if (numbers[0] >= 0)
                            {
                                seconds += numbers[0];
                            }

                            if (numbers[1] >= 0)
                            {
                                seconds += (numbers[1] * 60);
                            }

                            if (numbers[2] >= 0)
                            {
                                seconds += (numbers[2] * 3600);
                            }

                            writer.write("<seconds>");
                            writer.write(Long.toString(seconds));
                            writer.write("</seconds>");
                        }
                        else
                        {
                            event.writeAsEncodedUnicode(writer);
                        }
                    }
                    else if (event.isProcessingInstruction() == true)
                    {
                        ProcessingInstruction pi = (ProcessingInstruction)event;

                        writer.write("<?" + pi.getTarget() + " " + pi.getData() + "?>");
                    }
                    else if (event.isEntityReference() == true)
                    {
                        String entityName = ((EntityReference)event).getName();

                        writer.write("&");
                        writer.write(entityName);
                        writer.write(";");
                    }
                    else if (event.getEventType() == XMLStreamConstants.COMMENT)
                    {
                        writer.write("<!--" + ((Comment)event).getText() + "-->");
                    }
                    else if (event.getEventType() == XMLStreamConstants.DTD)
                    {
                        DTD dtd = (DTD) event;

                        if (dtd != null)
                        {
                            writer.write(dtd.getDocumentTypeDeclaration());
                        }
                    }
                    else if (event.isEndDocument() == true)
                    {

                    }
                }
            }
            catch (XMLStreamException ex)
            {
                System.out.println("prepare_input_1: An error occurred while reading input file \"" + inputFile.getAbsolutePath() + "\".");
                ex.printStackTrace();
                System.exit(1);
            }
            catch (SecurityException ex)
            {
                System.out.println("prepare_input_1: An error occurred while reading input file \"" + inputFile.getAbsolutePath() + "\".");
                ex.printStackTrace();
                System.exit(1);
            }
            catch (IOException ex)
            {
                System.out.println("prepare_input_1: An error occurred while reading input file \"" + inputFile.getAbsolutePath() + "\".");
                ex.printStackTrace();
                System.exit(1);
            }

            writer.flush();
            writer.close();
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("prepare_input_1: An error occurred while writing output file \"" + outputFile.getAbsolutePath() + "\".");
            ex.printStackTrace();
            System.exit(1);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("prepare_input_1: An error occurred while writing output file \"" + outputFile.getAbsolutePath() + "\".");
            ex.printStackTrace();
            System.exit(1);
        }
        catch (IOException ex)
        {
            System.out.println("prepare_input_1: An error occurred while writing output file \"" + outputFile.getAbsolutePath() + "\".");
            ex.printStackTrace();
            System.exit(1);
        }
    }
}
